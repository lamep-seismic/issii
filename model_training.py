///
A LAMEP BASED PROJECT - SEISMIC AUTOMATIC CLASSIFICATOR

PROJECTED AND CODED BY THE GROUP

@Rafael Magalhães
@Fabrício Sousa
@Edvaldo Melo
@Moisés Santos
@Gustavo Peixoto

///

import pandas as pd
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import GridSearchCV

# lendo os dados de entrada e saida
entrada = pd.read_excel('Entrada.xlsx')
saida = pd.read_excel('Saida.xlsx')

# Funcao para criacao da RNA
def criarRede(neuronios1, neuronios2, ativacao1, ativacao2, pesos, taxaAprendizagem, 
              decrescimo, dropout1, dropout2):
    rede = Sequential()
    # Adicionando a primeira camada oculta
    rede.add(Dense(units = neuronios1, activation = ativacao1,
                   kernel_initializer = pesos,
                   input_dim = 44))
    rede.add(Dropout(dropout1))
    # Adicionando a segunda camada oculta
    rede.add(Dense(units = neuronios2, activation = ativacao2,
                   kernel_initializer = pesos))
    rede.add(Dropout(dropout2))
    # Adicionando a camada de saída
    rede.add(Dense(units = 1, activation = 'sigmoid',
                   kernel_initializer = pesos))
    otimizador = keras.optimizers.Adam(lr = taxaAprendizagem, decay = decrescimo,
                                       clipvalue = 0.5)
    # Configuracao da rede
    rede.compile(optimizer = otimizador, loss = 'binary_crossentropy',
                 metrics = ['binary_accuracy'])
    return rede

rede = KerasClassifier(build_fn = criarRede)

# Parametros que serao testados
parametros = {'batch_size':[10,30,50], 'epochs':[500],
              'neuronios1':[30,50,80,128],
              'neuronios2':[30,50,80,128],
              'ativacao1':['relu', 'tanh'], 
              'ativacao2':['relu', 'tanh'],
              'pesos':['random_uniform', 'normal'],
              'taxaAprendizagem':[0.001, 0.006, 0.01, 0.03],
              'decrescimo':[0.0001, 0.001],
              'dropout1':[0.2,0.25,0.3],
              'dropout2':[0.2,0.25,0.3]}

# Fazendo o tuning
grid_search = GridSearchCV(estimator = rede, param_grid = parametros,
                           scoring = 'accuracy', cv = 5)
grid_search = grid_search.fit(entrada, saida)
melhores_parametros = grid_search.best_params_
melhor_precisao = grid_search.best_score_

